/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kopipagi.aplikasiabsensi.dao;

import com.kopipagi.aplikasiabsensi.entity.Absensi;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author agung
 */
public interface AbsensiDao extends PagingAndSortingRepository<Absensi, String>{
    
}
