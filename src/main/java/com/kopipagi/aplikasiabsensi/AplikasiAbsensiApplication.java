package com.kopipagi.aplikasiabsensi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplikasiAbsensiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiAbsensiApplication.class, args);
	}
}
