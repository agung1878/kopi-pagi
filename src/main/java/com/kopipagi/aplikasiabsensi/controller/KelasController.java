/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kopipagi.aplikasiabsensi.controller;

import com.kopipagi.aplikasiabsensi.dao.KelasDao;
import com.kopipagi.aplikasiabsensi.entity.Kelas;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author agung
 */

@Controller @RequestMapping("/kelas")
public class KelasController {
    
    @Autowired
    private KelasDao kelasDao;
    
    @GetMapping("/list")
    public String showList(ModelMap mm, @PageableDefault(size = 10) Pageable page){
        
        Page<Kelas> result = kelasDao.findAll(page);
        mm.addAttribute("data", result);
        
        return "kelas/list";
    
    }
    
    @ModelAttribute("pageTitle")
    public String pageTitle() {
        
        return "Kelas";
    }
    
    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) String id, ModelMap mm){
        
        Kelas kelas = new Kelas();
        
        if(StringUtils.hasText(id)){
            Optional<Kelas> k = kelasDao.findById(id);
            if(k.isPresent()){
                kelas = k.get();
            }
        }
        
        mm.addAttribute("kelas", kelas);
        
        return "/kelas/form";
    }
    
}
