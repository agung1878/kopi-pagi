package com.kopipagi.aplikasiabsensi.controller;

import com.kopipagi.aplikasiabsensi.dao.SiswaDao;
import com.kopipagi.aplikasiabsensi.entity.Siswa;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller @RequestMapping("/siswa")
public class SiswaController {
    
    @Autowired
    private SiswaDao siswaDao;
    
    
    @GetMapping("/list")
    public String daftar(ModelMap mm, @PageableDefault(size = 10) Pageable page){
        
        Page<Siswa> result = siswaDao.findAll(page);
        
        
        mm.addAttribute("data", result);
        return "siswa/list";
    
    }
    
    @ModelAttribute("pageTitle")
    public String pageTitle() {
        return "Daftar Siswa";
    }
    
    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) String id, ModelMap mm){
        
        Siswa siswa = new Siswa();
        
        if(StringUtils.hasText(id)){
            Optional<Siswa> s = siswaDao.findById(id);
            if(s.isPresent()){
                siswa = s.get();
            }
        }
        
        mm.addAttribute("siswa", siswa);
        
        return "siswa/form";
    }
    
    @PostMapping("/form")
    public String prosesForm(@ModelAttribute @Valid Siswa siswa, BindingResult errors, ModelMap mm){
        
        if(errors.hasErrors()){
            mm.addAttribute("siswa", siswa);
            return "siswa/form";
        }
        
        siswaDao.save(siswa);
        
        
        return "redirect:/siswa/list";
    }
    
}
