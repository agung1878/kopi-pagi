/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kopipagi.aplikasiabsensi.controller;

import com.kopipagi.aplikasiabsensi.dao.AbsensiDao;
import com.kopipagi.aplikasiabsensi.entity.Absensi;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author agung
 */

@Controller @RequestMapping("/absen")
public class AbsensiController {
    
    @Autowired
    private AbsensiDao absensiDao;
    
    @GetMapping("/list")
    public String showList(ModelMap mm, @PageableDefault(size = 10) Pageable page){
        
        Page<Absensi> result = absensiDao.findAll(page);
        mm.addAttribute("data", result);
    
        return "absen/list";
    
    }
    
    @ModelAttribute("pageTitle")
    public String pageTitle() {
        
        return "Daftar Absen";
    }
    
    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) String id, ModelMap mm){
        
        Absensi absensi = new Absensi();
        
        if(StringUtils.hasText(id)){
            Optional<Absensi> a = absensiDao.findById(id);
            if(a.isPresent()){
                absensi = a.get();
            }
        }
        
        mm.addAttribute("absensi", absensi);
        
        return "/absen/form";
    }
    
}
