/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kopipagi.aplikasiabsensi.controller;

import com.kopipagi.aplikasiabsensi.dao.JurusanDao;
import com.kopipagi.aplikasiabsensi.entity.Jurusan;
import java.util.Optional;
import jdk.nashorn.internal.runtime.options.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author agung
 */

@Controller @RequestMapping("/jurusan")
public class JurusanController {
    
    @Autowired
    private JurusanDao jurusanDao;
    
    @GetMapping("/list")
    public String showList(ModelMap mm, @PageableDefault(size = 10) Pageable page){
        
        Page<Jurusan> result = jurusanDao.findAll(page);
        
        mm.addAttribute("data", result);
    
        return "jurusan/list";
    
    }
    
    @ModelAttribute("pageTitle")
    public String pageTitle() {
        
        return "Jurusan";
    }
    
    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) String id, ModelMap mm){
        
        Jurusan jurusan = new Jurusan();
        
        if (StringUtils.hasText(id)){
            Optional<Jurusan> j = jurusanDao.findById(id);
            
            if(j.isPresent()){
                jurusan = j.get();
            }
        }
        
        mm.addAttribute("jurusan", jurusan);
        
        return "/jurusan/form";
    }
    
}
